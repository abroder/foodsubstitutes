#!/usr/bin/env node

var cheerio = require('cheerio')
var request = require('request')
var fs = require('fs')
var Twit = require('twit')
var T = new Twit(require('./config.js'))

const soylentPattern = /[sj]oylent( \d((\.\d)|\b))?/gi
const coffiestPattern = /coffiest/gi

const config = {
  subreddit: 'soylent',
  rules: [
    soylentPattern,
    coffiestPattern,
  ],
  substitutes: [
    {find: soylentPattern, replace: 'food'},
    {find: /\s+/g, replace: ' '},
    {find: coffiestPattern, replace: 'coffee'},
    {find: /drink/gi, replace: 'eat'},
    {find: /drank/gi, replace: 'ate'},
  ]
}

// `tweets` keeps track of any post titles that we find and process
var tweets = []
// `tweeted` keeps track of post titles that have already been tweeted; it's
// read in from a file on startup 
var tweeted = fs.readFileSync(__dirname + '/tweeted.txt', {flag: 'a+'}).
  toString().split('\n')

// Download all the posts currently on the "new" page of the Soylent subreddit
// and replace every instance of the word "Soylent" with the word "Food"
var updatePosts = function () {
  return new Promise(function (fulfill, reject) {
    request(`https://www.reddit.com/r/${config.subreddit}/new/?limit=100`, function (err, res, html) {
      if (err || res.statusCode != 200) {
        reject(err)
        return
      }

      var $ = cheerio.load(html)
      $('div.entry').each(function (i, elem) {
        var link = $(this).children('.top-matter').children('.title').children('a')
        var title = link.text()

        if (config.rules.some(rule => title.match(rule))) {
          config.substitutes.forEach(({find, replace}) => {
            title = title.replace(find, replace)
          })

          // For the most part, the bot doesn't care about maintaining case;
          // the one exception is that the beginning of each headline is 
          // capitalized
          if (title[0] === 'f') {
            title = 'F' + title.substring(1)
          }

          // Add the title to `tweets` 
          tweets.push(title)
        }
      })
      
      fulfill()
    })
  })
}

var tweet = function () {
  if (tweets.length === 0) {
    console.log('No tweets available; waiting until later to try again.')
    return
  }

  var title = tweets.splice(Math.floor(Math.random() * tweets.length), 1)[0]
  if (tweeted.indexOf(title) !== -1) {
    // If we've already tweeted this title, try selecting another title to 
    // tweet
    console.log(`Already tweeted: '${ title }'. Retrying...`)
    tweet()
    return
  }

  fs.appendFileSync(__dirname + '/tweeted.txt', title + '\n');

  T.post('statuses/update', { status: title }, function (err, data, response) {
   if (err) {
     console.log(err)
     return
   }
  })
}

updatePosts().
then(function () {
  tweet()
}).
catch(function (err) {
  console.log(err)
})
